var passport = require("passport");
var basicStrategy = require("passport-http").BasicStrategy;
var assert = require("assert");
var sys_username = require("username").sync();
var pam = require('authenticate-pam');

module.exports = function (options, imports, register) {

    passport.use(new basicStrategy({realm: "CustomAuthSystem"}, function(username, password, done) {
        // TODO auth system goes here
        if(username != sys_username) {
            // TODO do we want to do something else?
            return done(null, false);
        }

        pam.authenticate(sys_username, password, function(err) {
            if(err) {
                console.log("[note] Authentication failure: " + err);
                return done(null, false);
            } else {
                return done(null, username);
            }
        });
    }));

    var connect = imports.connect;

    connect.useSetup(passport.initialize());
    connect.useSetup(passport.authenticate('basic', { session: false }), function(req, res) {});

    console.log("Using PAM Authentication");

    register();
};

